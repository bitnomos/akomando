// libs
import { DOMImplementation } from 'xmldom';
import { XMLSerializer } from 'xmldom';
import xml2js from 'xml2js';
import _ from 'underscore';

/**
 * Cleans a DOM node by removing all the unuseful spaces
 * @param {Node} node The DOM node that must be cleaned
*/
export const _cleanNode = node => {
   for(var n = 0; n < node.childNodes.length; n ++){
      var child = node.childNodes[n];
      if(child.nodeType === 8 || (child.nodeType === 3 && !/\S/.test(child.nodeValue))){
         node.removeChild(child);
         n --;
      }
      else if(child.nodeType === 1){
         _cleanNode(child);
      }
   }
   return node;
};

/**
 * Translate an akomantoso fragment into HTML
 * @todo Handle all types of nodes.
 * @param {Document} XMLDom The AkomaNtoso XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
 * Document</a> fragment to translate
 * @param {Document} HTMLDom The contestual XML <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/API/Node">
 * Node</a> from wich the method must start the translation
 * @param {Document} domDocument The HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
 * Document</a> to wich append the results
 * @return {Object} The new HMTL <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
 * Document</a>  given in input but filled with translated elements
*/
export const _transformDomToHTML = (XMLDom,HTMLDom,domDocument) => {
   const node = XMLDom;
   const nodeType = node.nodeType;

   switch(nodeType){
   // process document node
   case 9:
      _transformDomToHTML(node.firstChild,HTMLDom,domDocument);
      break;
   // process nodes
   case 1:
      // transform the element
      var divElement = domDocument.createElement('div');
      divElement.appendChild(domDocument.createTextNode(''));
      divElement.setAttribute('class', `akn-${node.nodeName}`);
      HTMLDom.appendChild(divElement);

      // transform element's attributes
      if (node.hasAttributes && node.hasAttributes()) {
         const nodeAttributes = node.attributes;
         for (var attribute = 0; attribute < nodeAttributes.length; attribute++) {
            var attName = 'data-akn-' + XMLDom.attributes[attribute].name.replace('ndiff:', '');
            var attValue = XMLDom.attributes[attribute].value;
            divElement.setAttribute(attName, attValue);
         }
      }

      // transform element's children
      var nodeChildren = node.childNodes;
      for (var child = 0; child < nodeChildren.length; child++) {
         _transformDomToHTML(nodeChildren[child],divElement,domDocument);
      }
      break;
   // process text nodes
   case 3:
      var textValue = node.nodeValue.replace(/(\r\n|\n|\r)/gm, '').replace(/\s+/g, ' ');
      var spanElement = domDocument.createElement('span');
      spanElement.setAttribute('class', 'akn-text-fragment');
      spanElement.appendChild(domDocument.createTextNode(`${textValue}`));
      HTMLDom.appendChild(spanElement);
      break;
   case 7:
      _transformDomToHTML(node.nextSibling,HTMLDom,domDocument);
      break;
   default:
      break;
   }
   return domDocument;
};

/**
 * Transforms an XML string into a JSON
 * @todo Find a better way to do this. Replace the callback with asyinc/await?
 * @param {String} anXMLString The xml string that must be transformed to JSON
 * @return {Object} The JSON serialization of the xml string
*/
export const _transformDomToJSON = anXMLString => {
   let results;
   xml2js.parseString(anXMLString, {
      preserveChildrenOrder : true,
   }, (err, result) => {
      if(err){
         results = err;
      }
      results =result;
   });
   return results;
};

/**
 * Transforms a JSON into a DOM
 * @param {JSON} aJSON The JSON that must be transformed to XML
 * @return {Object} The XML serialization of the given JSON
*/
export const _transformJSONToDom = aJSON => {
   const builder = new xml2js.Builder();
   return builder.buildObject(aJSON);
};

/**
 * Strips a prefix from a tag name if it is present
 * @param {String} tagWitPrefix The tag name with the prefix that must be stripped
 * @return {String} The tag name without the prefix
*/
export const _stripPrefix = tagWitPrefix => {
   const tokens = tagWitPrefix.split(':');
   return (tokens.length > 1) ? tokens[1] : tagWitPrefix;
};

/**
 * @typedef {Object}  HTMLDocument
 * @property {Object} HTMLDocument.htmlDocument An HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
 * Document</a>
 * @property {Object} HTMLDocument.head A reference to the head of the html HTML document
 * @property {Object} HTMLDocument.body - A reference to the body of the created HTML document
*/
/**
 * Creates an HTML document
 * @param {Object} options - The options for the created HTML document
 * @param {String} [options.addDocTypeDeclaration=true] - Set it to true to add the html doctype declaration into the created document
 * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
 * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
 * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
 * @return {HTMLDocument} The created HTML document
*/
export const _createHTMLDocument = function({
   addDocTypeDeclaration = true,
   addHtmlElement = true,
   addHeadElement = true,
   addBodyElement = true,
} = {}){
   let htmlDocument;
   let head;
   let body;
   const dom = new DOMImplementation();
   if(addHtmlElement){
      const doctype = dom.createDocumentType('html');
      htmlDocument = dom.createDocument(null, 'html', (addDocTypeDeclaration ? doctype : ''));
   }
   else{
      htmlDocument = dom.createDocument(null,'div');
      htmlDocument.documentElement.setAttribute('class','akn-to-html-result');
   }
   if(addHeadElement){
      head = htmlDocument.createElement('head');
      htmlDocument.documentElement.appendChild(head);
   }
   if(addBodyElement){
      body = htmlDocument.createElement('body');
      htmlDocument.documentElement.appendChild(body);
   }
   else{
      body = htmlDocument.createElement('div');
      body.setAttribute('class','akn-fragment');
      htmlDocument.documentElement.appendChild(body);
   }
   return {
      htmlDocument: htmlDocument,
      head: head,
      body: body,
   };
};

/**
 * Serializes a DOM document into a string
 * @param {Object} aDom - The dom document that must be serialized into a string
 * @return {String} The string serialization of the DOM
*/
export const _serializeDomToString = aDom => {
   const serializer = new XMLSerializer();
   return serializer.serializeToString(aDom);
};


/**
 * Returns an array containing the list of the given nodes serialized in the given serialization
 * @param {Array} nodes - An array of DOM nodes
 * @param {String} serialize - The requested serialization
 * @return {Array} An array containing all the given nodes in the requested serialization
*/
export const _serializeNodes = (nodes, serialize, ...options) => {
   /**
    * @todo Luca Cervone investigate this one. I do not know if it is useful and it is surely broken when
    * html is required
   */
   if(options[0]['insertDataXPath']){
      for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
         _setDataAkomandoXpathAttribute(nodes[nodeIndex]);
      }
   }

   let results = [];
   switch(serialize){
   case 'AKNDom':
      for (var nodeC1 = 0; nodeC1 < nodes.length; nodeC1++) {
         results.push(nodes[nodeC1]);
      }
      break;
   case 'AKNString':
      for (var nodeC2 = 0; nodeC2 < nodes.length; nodeC2++) {
         const aknFragmentString = _serializeDomToString(nodes[nodeC2]);
         results.push(aknFragmentString);
      }
      break;
   case 'HTMLDom':
      for (var nodeC3 = 0; nodeC3 < nodes.length; nodeC3++) {
         const {htmlDocument, body} = _createHTMLDocument({
            addHtmlElement: (options[0]['addHtmlElement']) ? options[0]['addHtmlElement'] : false,
            addHeadElement: (options[0]['addHeadElement']) ? options[0]['addHeadElement'] : false,
            addBodyElement: (options[0]['addBodyElement']) ? options[0]['addBodyElement'] : false,
         });
         const htmlFragment = _transformDomToHTML(nodes[nodeC3],body,htmlDocument);
         results.push(htmlFragment);
      }
      break;
   case 'HTMLString':
      for (var nodeC4 = 0; nodeC4 < nodes.length; nodeC4++) {
         const {htmlDocument, body} = _createHTMLDocument({
            addHtmlElement: (options[0]['addHtmlElement']) ? options[0]['addHtmlElement'] : false,
            addHeadElement: (options[0]['addHeadElement']) ? options[0]['addHeadElement'] : false,
            addBodyElement: (options[0]['addBodyElement']) ? options[0]['addBodyElement'] : false,
         });
         const htmlFragment = _transformDomToHTML(nodes[nodeC4],body,htmlDocument);
         const aknFragmentString = _serializeDomToString(htmlFragment);
         results.push(aknFragmentString);
      }
      break;
   case 'JSON':
      for (var nodeC5 = 0; nodeC5 < nodes.length; nodeC5++) {
         const aknFragmentString = _serializeDomToString(nodes[nodeC5]);
         results.push(_transformDomToJSON(aknFragmentString));
      }
      break;
   case 'JSONString':
      for (var nodeC6 = 0; nodeC6 < nodes.length; nodeC6++) {
         const aknFragmentString = _serializeDomToString(nodes[nodeC6]);
         results.push(JSON.stringify(_transformDomToJSON(aknFragmentString)));
      }
      break;
   default:
      return;
   }
   return results;
};


/**
 * Get the full path of a hierarchy until the main container of its document type
 * @param {Object} node The hierarchy whose path is requested
 * @param {String} mainContainer The main container of the document containing the hier
 * @param {Array} path The initial path
 * @return {Array} An array containing the full path of the hierachy
*/
export const _getHierPath = (node, mainContainer, path=[]) => {
   const nodePosition = _getNodePosition(node,0);
   if(node.parentNode.nodeName===mainContainer){
      path.push({
         name: `${node.nodeName}`,
         position: nodePosition,
      });
      path.push({
         name: mainContainer,
         position: 1
      });
   }
   else{
      path.push({
         name: `${node.nodeName}`,
         position: nodePosition,
      });
      _getHierPath(node.parentNode, mainContainer, path);
   }
   return path;
};

/**
 * Get the position of a node inside its parent
 * @param {Object} node The node to inspect
 * @param {Object} [position=1] The initial position to start the recursion
 * @return {Integer} The node poistion
*/
export const _getNodePosition = (node, position=0) => {
   position++;
   const previousSibling = _getPreviousSibling(node);
   const nodePrevious = (previousSibling) ? previousSibling : null;
   if(nodePrevious && nodePrevious.nodeName === node.nodeName){
      position = _getNodePosition(nodePrevious,position);
   }
   return position;
};

/**
 * Get the hierachies contained in a set of elements
 * @param {String} docTypeMainContainer The name of the main container of a doc type
 * @param {String} hiersContentElement The name of the content element in hierarchies
 * @param {Array} hierBlockElements The block elements that can contain other structures (quotedStructure) in hierarchies
 * @param {Array} nodes The set of nodes in which hierarchies must be found
 * @param {Array} hierarchiesNames The names of the hierarchies that may be contained in a specific doc type
 * @param {Array} hiers The returning array that is filled recursively with found hierarchies
 * @return {Array} An array containing all hierarchies that have been found inside the supplied node set
*/
export const _getDocumentHiers = (
   docTypeMainContainer,
   hiersContentElement,
   hierBlockElements,
   nodes,
   hierarchiesNames,
   hiers=[]) => {
   for (var i =0; i < nodes.length; i++){
      const nodeName = nodes[i].nodeName;
      if (nodeName === docTypeMainContainer || _.contains(hierarchiesNames,nodeName)){
         if(nodeName != docTypeMainContainer &&
            nodeName != hiersContentElement &&
            !_.contains(hierBlockElements,nodeName)){
            hiers.push(nodes[i]);
         }
         if (nodes[i].hasChildNodes()){
            const children = nodes[i].childNodes;
            _getDocumentHiers(docTypeMainContainer,
               hiersContentElement,
               hierBlockElements,
               children,
               hierarchiesNames,
               hiers);
         }
      }
   }
   return hiers;
};

/**
 * Returns an Object containing the identifiers of the owner document of a node
 * @param {Object} identifiers An extract of the {@link akomando.config} Object containing information on identifiers
 * @param {Object} node The node whose owner document identifiers are requested
 * @return {Object} The identifiers of the owner document of the node
*/
/*export const _getNodeOwnerIdentifiers = function(identifiers, node){
   const documentIdentifiers = {
      collection: {},
      document: {},
   };
   const nodeOwnerDocument = node.ownerDocument.documentElement;
   const ids = _.pluck(identifiers.identifiers,'name');
   for (let id of ids){
      const identifierElement = nodeOwnerDocument.getElementsByTagName(id)[0];
      const identifier = (identifierElement) ? identifierElement.getElementsByTagName(identifiers.FRBRthis.name)[0].getAttribute('value').value : '';
      documentIdentifiers.document[id] = identifier;
   }
   return documentIdentifiers;
};*/

/**
 * Given an element it returns the {@link akomando.hier.identifier} object describing the element
 * @param {Object} node The node that must be described
 * @return {Object} The {@link akomando.hier.identifier} object describing the node
*/
export const _getHierIdentifierObject = (node, mainContainer) => {
   let hierPath = (() => {
      const nodePathObject = _getHierPath(node, mainContainer);
      let myPath = [];
      while(nodePathObject.length > 0){
         const pathToken = nodePathObject.pop();
         myPath.push(`${pathToken.name}[${pathToken.position}]`);
      }
      return `/${myPath.join('/')}`;
   })();
   let hierLevel = hierPath.split('/').length - 2;

   return Object.assign(
      _getIdentifierObject(node),
      {
         nameAttribute: node.hasAttribute('name') ? node.getAttribute('name') : '',
         xpath: hierPath,
         level: hierLevel,
      }
   );
};

/**
 * Given an element it returns the {@link akomando.identifier} object describing the element
 * @param {Object} node The node that must be described
 * @return {Object} The {@link akomando.identifier} object describing the node
*/
export const _getIdentifierObject = node => {
   return {
      name: node.nodeName,
      eId: node.hasAttribute('eId') ? node.getAttribute('eId') : '',
      wId: node.hasAttribute('wId') ? node.getAttribute('wId') : '',
      GUID: node.hasAttribute('GUID') ? node.getAttribute('GUID') : '',
      xpath: _getXpath(node),
   };
};

/**
 * Given an element it returns the {@link akomando.text.identifier} object describing the element
 * @param {Object} node The node that must be described
 * @return {Object} The {@link akomando.text.identifier} object describing the node
*/
export const _getTextIdentifierObject = node => {
   const result = {
      name: 'text',
      text: node.textContent,
      xpath: _getXpath(node),
   };

   return node.nodeType === node.ELEMENT_NODE ?
      Object.assign(result, _getIdentifierObject(node)) : result;
};


/**
 * Given an element it returns its xpath
 * @param {Object} node The node that must be described
 * @return {String} the xpath of the node as a string
*/
export const _getXpath = node => {
   const nodes = [];
   while(node && node.nodeType !== node.DOCUMENT_NODE) {
      nodes.push({
         name: node.nodeName,
         pos: _getNodePosition(node, 0),
      });
      node = node.parentNode;
   }
   const paths = nodes
      .reverse()
      .map((node) => `${node.name}[${node.pos}]`);
   return `/${paths.join('/')}`;
};

/**
 * Given an element it sets the data-akomando-xpath attribute to it and to its children recursively
 * @param {Object} node The node to which the attribute must be added
*/
export const _setDataAkomandoXpathAttribute = node => {
   if (!node) return;
   const nodeXPath = _getXpath(node);
   if (node.nodeType === node.ELEMENT_NODE) {
      node.setAttribute('data-akomando-xpath', nodeXPath);
      for (var childIndex = 0; childIndex <= node.childNodes.length; childIndex++){
         _setDataAkomandoXpathAttribute(node.childNodes[childIndex]);
      }
   }
};

/**
 * Given an element it returns all the contained text from itself to all discentants
 * @param {Object} node The node that must be described
 * @return {Array} An array describing al the text objects of the documents as {@link akomando.text.identifier}
*/
export const _getDocumentElementsIdentifier = node => {
   let textObjcts = [];
   for (var i = 0; i < node.childNodes.length; i++) {
      const chNode = node.childNodes[i];
      if (chNode.nodeType !== node.PROCESSING_INSTRUCTION_NODE &&
            chNode.nodeType !== node.COMMENT_NODE &&
            chNode.nodeType !== node.DOCUMENT_NODE) {
         textObjcts.push(_getTextIdentifierObject(chNode));
      }
      if (chNode.nodeType === node.ELEMENT_NODE) {
         textObjcts = textObjcts.concat(_getDocumentElementsIdentifier(chNode));
      }
   }
   return textObjcts;
};

/**
 * Given an element it returns all its attributes
 * @param {Object} node The node from which the attributes must be extracted
 * @return {Object} An object with the attributes of the node and its childrenn as {@link akomando.elements.attributes}
*/
export const _getElementAttributes = node => {
   if (!node.hasAttributes()){
      return [];
   }
   const nodeAttributes = node.attributes;
   let nodeAttributesList = [];
   for (var i = 0; i < nodeAttributes.length; i++) {
      nodeAttributesList.push({
         name: nodeAttributes[i].name,
         value: nodeAttributes[i].value,
      });
   }
   return nodeAttributesList;
};

/**
 * Given an element it returns all its attributes and, if deep is set tu true,
 * it retursn all the attributes of its childs recursively
 * @param {Object} node The node that must be described
 * @return {Object} An object with the attributes of the node and its childrenn as {@link akomando.elements.attributes}
*/
export const _getElementsAttributes = node => {
   let elementsAttributesList = [];
   for (var i = 0; i < node.childNodes.length; i++) {
      const chNode = node.childNodes[i];
      if (chNode.nodeType !== node.PROCESSING_INSTRUCTION_NODE &&
            chNode.nodeType !== node.COMMENT_NODE &&
            chNode.nodeType !== node.DOCUMENT_NODE &&
            chNode.nodeType !== node.TEXT_NODE) {
         elementsAttributesList.push({
            nodeName: chNode.nodeName,
            attributesList: _getElementAttributes(chNode),
            nodeInfo: _getTextIdentifierObject(chNode),
         });
      }
      if (chNode.nodeType === node.ELEMENT_NODE) {
         elementsAttributesList = elementsAttributesList.concat(_getElementsAttributes(chNode));
      }
   }
   return elementsAttributesList;
};

/**
 * Get the previous node sibling of a given node
 * @param {Object} node The node whose previous sibling is required
 * @return {Object} The previous sibling of the node
*/
export const _getPreviousSibling = node => {
   var x = node.previousSibling;
   while (x && x.nodeType != 1) {
      x = x.previousSibling;
   }
   return x;
};

/**
 * Improved dom.getElementsByTagName method that you can call with or without namespace
 * @param {String} context the context in which elments that must be found
 * @param {String} elementName the name of the elments that must be found
 * @param {String} namespace the namespace
 * @return {Object} the nodeList containing the results of the query
*/

export const _getElementsByTagNameUniversal = (context, elementName, namespace=null) => (
   (context) ?
      (namespace) ?
         context.getElementsByTagNameNS(namespace, elementName) :
         context.getElementsByTagName(elementName) :
      []
);

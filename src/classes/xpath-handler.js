/**
 * A class to handle XPath queries on AkomaNtoso document.
 * The XPath considered is actually a simplified version, so that we don't consider
 * attributes handling (with [@attr]) nor arbitrary location (with //)
 */
export default class _XPathHandler {

   /**
    * Search in the XML DOM all the elements that match the given XPath
    * @param {String} xpath A string representing the XPath (in a simplified version without attributes and // )
    * @param {Object} dom The Document (XML DOM) in which we are looking for
    * @param {Boolean} ignorePrefix If true select the elements ignoring their prefix (default: false)
    * @return {Object} A list of the XML elements that match the XPath
    */
   static select(xpath, dom, ignorePrefix=false)
   {
       const tokens = _XPathHandler.tokenizeXPath(xpath);

       /**
        * Auxilliary function for searching recursively
        * node is the actual root of the DOM
        * tokens is the list of {@link xpath_handler.token} taken from the XPath
        * index is the position of the actual token we are looking at
        */
       function selectRec(node, tokens, index)
       {
           if(index >= tokens.length)
               return [node];

           let matches = [];
           let counter = 0;
           for(let i = 0; i < node.childNodes.length; i++)
           {
                if (node.childNodes[i].nodeType == 1){
                    const tagName = (ignorePrefix) ?
                        node.childNodes[i].tagName.split(':').slice(-1)[0] :    // take last part after ':'
                        node.childNodes[i].tagName;
                    if(tagName == tokens[index].element)  // check the name of the element
                    {
                    counter++;
                    if(tokens[index].index == undefined || counter == tokens[index].index)   // check the (eventual) index of the element
                            // recursive call
                            matches = matches.concat(selectRec(node.childNodes[i], tokens, index+1))
                    }
                }
           }
           return matches;
       }

       return selectRec(dom, tokens, 0);
   }

   /**
    * Object representing one of the token in a (simplified) XPath
    * @typedef {Object} xpath_handler.token
    * @property {String} element The name of the tag requested
    * @property {Int} index The index of the requested element
    */

    /**
     * Decompose an XPath in a list of token (after verifing the correctness)
     * @throws {Error} Throws error when the given string does not match the correct form of the simplified XPath
     * @param {String} xpath A string representing the XPath; must be completed and without attributes
     * @return {Object} A list of token as {@link xpath_handler.token}
     */
    static tokenizeXPath(xpath)
    {
        // test if it's a well formed xpath
        let regex = /^\/?((\w+:)?\w+(\[\d+\])?\/)*(\w+:)?\w+(\[\d+\])?$/;
        if(!regex.test(xpath))
            throw new Error(`invalid xpath: ${xpath}`);

        let tokensStr = xpath.split('/');
        tokensStr = tokensStr.filter((token) => token != "");
        let tokensObj = tokensStr.map(function(token) {
            let matches = token.match(/\[\d+\]/); // look for an index [.]
            if(matches && matches.length > 0)
            {
                let tag = token.substr(0, token.length - matches[0].length);    // tag name (without index)
                let ind = parseInt(matches[0].substr(1))    // get the index inside [.]
                return { element: tag, index: ind };
            }
            else return { element: token }
        });
        return tokensObj;
    }
}

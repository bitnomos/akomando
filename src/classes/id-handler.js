/**
 * A class that do semantic checks on the Akoma Ntoso document
 * The XPath considered is actually a simplified version, so that we don't consider
 * attributes handling (with [@attr]) nor arbitrary location (with //)
 */
export default class _IdHandler {

    constructor () {
        this.idMap = {}     // can handle multiple types of ids
    }

    /**
     * Index a new element in the idMap, for the id of the given name with the given value
     */
    insertId(idName, idValue, element)
    {
        if(this.idMap[idName] == undefined)     // need a new map for id with the name idName
            this.idMap[idName] = {};

        if(this.idMap[idName][idValue] == undefined)
            this.idMap[idName][idValue] = [element];
        else
            this.idMap[idName][idValue].push(element);
    }

    /**
     * Given an XML DOM, it saves every eId in the map eIdMap
     * @param {DOM} node The (already parsed) DOM of the document
     */
    findIds(node, idName)
    {
        if(this.idMap[idName] == undefined)
            this.idMap[idName] = {};    // inizialize the map, if empty
        
        // visit the DOM through recursive calls, saving the Ids in the object map
        if(node.nodeType == 1)
        {
            let id = node.getAttribute(idName);
            if(id != '')
                this.insertId(idName, id, node);
        }

        for(let i = 0; node.childNodes && i < node.childNodes.length; i++)
            //visit(node.childNodes[i], map);
            this.findIds(node.childNodes[i], idName);
    }

    /**
     * Given an XML tag, returns the relative element_ref, as stated in the Akoma Ntoso Naming Convention
     * @param {String} tag A valid Akoma Ntoso tag
     * @return {String} The official element_ref that the ID must contain
     */
    static elementMap(tag) {
        switch (tag) {
            // abbreviations
            case 'alinea':
                return 'al';
            case 'amendmentBody':
            case 'debateBody':
            case 'judgmentBody':
            case 'mainBody':
                return 'body';
            case 'article': return 'art';
            case 'attachment': return 'att';
            case 'blockList': return 'list';
            case 'chapter': return 'chp';
            case 'citation': return 'cit';
            case 'clause': return 'cl';
            case 'component': return 'cmp';
            case 'components': return 'cmpnts';
            case 'componentRef': return 'cref';
            case 'debateSection': return 'dbsect';
            case 'division': return 'dvs';
            case 'documentRef': return 'dref';
            case 'eventRef': return 'eref';
            case 'documentRef': return 'dref';
            case 'listIntroduction': return 'intro';
            case 'listWrapUp':
            case 'wrapUp':
                return 'wrap';
            case 'paragraph': return 'para';
            case 'quotedStructure': return 'qstr';
            case 'quotedText': return 'qtext';
            case 'recital': return 'rec';
            case 'recitals': return 'recs';
            case 'section': return 'sec';
            case 'subchapter': return 'subchp';
            case 'subclause': return 'subcl';
            case 'subdivision': return 'subdvs';
            case 'subparagraph': return 'subpara';
            case 'subsection': return 'subsec';
            case 'temporalGroup': return 'tmpg';

            // special cases with arbitrary label
            case 'TLCConcept':
            case 'TLCEvent':
            case 'TLCLocation':
            case 'TLCObject':
            case 'TLCOrganization':
            case 'TLCPerson':
            case 'TLCProcess':
            case 'TLCReference':
            case 'TLCRole':
            case 'TLCTerm':
            case 'componentData':
            case 'keyword':
            case 'component':
                return null;

            // default case
            default: return tag;
        }
    }


    /**
    * Check that the attributes eId and wId are well formed, that is respect the form [prefix “__”] element_ref [“_”number]
    * @param {Object} element An element of the document DOM
    * @param {String} idName The name of the attribute that contain the id
    * @return {Object} An {@link akomando.document.idError} object representing the error, or null
    */
    static checkId(element, idName)
    {
        const ELEMENT_TYPE = 1;
        if(element.nodeType != ELEMENT_TYPE)
            return null;

        const eId = element.getAttribute(idName);
        //const wId = element.getAttribute('wId');

        if(eId == '')
            return null;

        let parts = eId.split('__');

        // check the last part (actual element)
        let last = parts[parts.length - 1].split('_');

        let element_ref = _IdHandler.elementMap(element.tagName)
        if(element_ref != null && last[0] != element_ref)
        {
            //throw new Error(`Bad Id ${id}: expected identifier '${element_ref}', got '${last[0]}'`);
            let context = parts.slice(0, parts.length - 1).join('__');
            let index = last.slice(1).join('_');
            let suggestion = element_ref;
            if (context.length > 0)
                suggestion = context + '__' + suggestion;
            if (index.length > 0)
                suggestion = suggestion + '_' + index;
            return {
                element: element,
                message: `Bad id ${idName}="${eId}": expected identifier '${element_ref}', got '${last[0]}'`,
                idName: idName,
                value: eId,
                suggestion:  suggestion
            };
        }
        else return null;
    }


    /**
     * Check the correctness of all eId in the eIdMap
     * @param {String} idName The name of the id to check. Possible values are: 'all', 'eId', 'wId'
     * @return {Object} A list of {@link akomando.document.idError} representing bad ID values
     */
    checkAllIds(idName)
    {
        let map;
        if(idName == 'all')
            return this.checkAllIds('eId').concat(this.checkAllIds('wId'));
        else
            map = this.idMap[idName];

        let errList = [];

        Object.keys(map).forEach(function(key){
            for(let i=0; i < map[key].length; i++) {
                let err = _IdHandler.checkId(map[key][i], idName);
                if(err != null)
                    errList.push(err);
            }
        });

        return errList;
    }


    /**
     * Returns the elements with the given id, using a pre-computed map.
     * @param {String} idName The value of the attribute eId to search
     * @param {String} idValue The value of the attribute eId to search
     * @return {Object} A list of XML element with the given Id, if they exist, else undefined
     */
    getElementById(idName, idValue) {
        if (this.idMap[idName] != undefined)
            return this.idMap[idName][idValue];
        else
            return undefined;
    }
}

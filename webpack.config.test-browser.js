/* eslint-disable */
var webpack = require('webpack');
var path = require('path');

module.exports = {
   entry: path.resolve(__dirname, './test/browser/test-browser.js'),

   output: {
      path: path.resolve(__dirname, 'test/browser/dist'),
      filename: 'test-browser.api.min.js',
   },

   mode: 'production',

   target: 'web',

   module: {
      rules: [
         {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
               loader: 'babel-loader'
            }
         }
      ]
   },

   plugins: [
      new webpack.IgnorePlugin(/^fs$/),
      new webpack.IgnorePlugin(/akomando$/)
   ]
};

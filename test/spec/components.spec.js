import { expect } from 'chai';
import { getFile, createAkomando } from '../imports/utils'

/**
 * @todo getComponents must be fixed to return a string (not [object object]) for the cli version
 * @todo implement a test for getComponents on a document that is not a
 * @todo implement tests for document_docCollection_componentsEverywhere
 * documentCollection and contains components
 */

describe('#AkomantosoComponentsHandler Document document_it.xml (Italy)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_it.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 0 components', function () {
      const n = myAkomando.getComponents()[0].components.length;
      expect(n).to.equal(0);
   });
});

describe('#AkomantosoComponentsHandler Document document_docCollection.xml (Uruguaian qualified document collection)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_docCollection.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 4 components', function () {
      const n = myAkomando.getComponents()[0].components.length;
      expect(n).to.equal(4);
   });

   it('The first component has to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[0].component.isReferenceToComponent;
      expect(n).to.equal(true);
   });

   it('The third component has not to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[2].component.isReferenceToComponent;
      expect(n).to.equal(false);
   });

   it('The first component has not to be referenced internally ', function () {
      const n = myAkomando.getComponents()[0].components[0].component.isReferencedInternally;
      expect(n).to.equal(false);
   });

   it('The third component has not to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[2].component.isReferencedInternally;
      expect(n).to.equal(true);
   });
});

describe('#AkomantosoComponentsHandler Document document_docCollection_componentsEverywhere.xml', function () {
   let myAkomando;
   before((done) => {
      getFile('document_docCollection_componentsEverywhere.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 8 components', function () {
      const n = myAkomando.getComponents()[0].components.length;
      expect(n).to.equal(8);
   });

   it('The first component has to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[0].component.isReferenceToComponent;
      expect(n).to.equal(true);
   });

   it('The fourth component has not to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[3].component.isReferenceToComponent;
      expect(n).to.equal(false);
   });

   it('The first component has not to be referenced internally ', function () {
      const n = myAkomando.getComponents()[0].components[0].component.isReferencedInternally;
      expect(n).to.equal(false);
   });

   it('The seventh component has not to be "referenceToComponent" ', function () {
      const n = myAkomando.getComponents()[0].components[6].component.isReferencedInternally;
      expect(n).to.equal(true);
   });

   it('The last component has not to be the last in the whole document ', function () {
      const n = myAkomando.getComponents()[0].components[7].component.positionInWholeDocument;
      expect(n).to.equal(8);
   });

   it('The last component has not to be the secondo in the owner document ', function () {
      const n = myAkomando.getComponents()[0].components[7].component.positionInOwnerDocument;
      expect(n).to.equal(2);
   });
});

import { expect } from 'chai';
import { describe } from 'mocha';
import { it } from 'mocha';
import { execSync } from 'child_process';
import { DOMParser } from 'xmldom';
import { pd } from 'pretty-data2';

describe('#AkomantosoComponentsHandler Document document_it.xml (Italy)', function () {

   it('It has to find 0 components', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_it.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components.length;
      expect(n).to.equal(0);
   });
});


describe('#AkomantosoComponentsHandler Document document_docCollection.xml (Uruguaian qualified document collection)', function () {

   it('It has to find 4 components', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components.length;
      expect(n).to.equal(4);
   });

   it('The first component has to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[0].component.isReferenceToComponent;
      expect(n).to.equal(true);
   });

   it('The third component has not to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[2].component.isReferenceToComponent;
      expect(n).to.equal(false);
   });

   it('The first component has not to be referenced internally ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[0].component.isReferencedInternally;
      expect(n).to.equal(false);
   });

   it('The third component has not to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[2].component.isReferencedInternally;
      expect(n).to.equal(true);
   });
})

describe('#AkomantosoComponentsHandler Document document_docCollection_componentsEverywhere.xml', function () {

   it('It has to find 8 components', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components.length;
      expect(n).to.equal(8);
   });

   it('The first component has to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[0].component.isReferenceToComponent;
      expect(n).to.equal(true);
   });

   it('The fourth component has not to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[3].component.isReferenceToComponent;
      expect(n).to.equal(false);
   });

   it('The first component has not to be referenced internally ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[0].component.isReferencedInternally;
      expect(n).to.equal(false);
   });

   it('The seventh component has not to be "referenceToComponent" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[6].component.isReferencedInternally;
      expect(n).to.equal(true);
   });

   it('The last component has not to be the last in the whole document ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[7].component.positionInWholeDocument;
      expect(n).to.equal(8);
   });

   it('The last component has not to be the secondo in the owner document ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-components test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      const n = execResult[0].components[7].component.positionInOwnerDocument;
      expect(n).to.equal(2);
   });
})

describe('#AkomantosoReferencesHandler Document document_it.xml (Italy)', function () {

   it('It has to find 91 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_it.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "number" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_it.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_it.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_it.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_it.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_it.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_it.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_it.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });

})

describe('#AkomantosoReferencesHandler Document document_uy.xml (Uruguay)', function () {

   it('It has to find 7 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_uy.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(7);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_uy.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_uy.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_uy.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_uy.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "limeEditor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_uy.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first unused reference has to be "/lime.cirsfid.unibo.it" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_uy.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last unused reference has to be "Editor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_uy.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[6].showAs;
      expect(showAs).to.equal('Editor');
   });

})

describe('#AkomantosoReferencesHandler document_unqualified.xml(Italy and unqualified elements)', function () {

   it('It has to find 91 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_unqualified.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_unqualified.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_unqualified.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_unqualified.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_unqualified.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_unqualified.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_unqualified.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_unqualified.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });


})


describe('#AkomantosoReferencesHandler document_qualifiedWithPrefix.xml (Italy and qualified elements)', function () {

   it('It has to find 91 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_qualifiedWithPrefix.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_qualifiedWithPrefix.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_qualifiedWithPrefix.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_qualifiedWithPrefix.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_qualifiedWithPrefix.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_qualifiedWithPrefix.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_qualifiedWithPrefix.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_qualifiedWithPrefix.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });

})

describe('#AkomantosoReferencesHandler Document document_docCollection.xml(Uruguaian qualified document collection)', function () {

   it('It has to find 9 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(9);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_docCollection.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "limeEditor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first unused reference has to be "/lime.cirsfid.unibo.it" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last unused reference has to be "Editor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[3].showAs;
      expect(showAs).to.equal('Editor');
   });


})

describe('#AkomantosoReferencesHandler Document document_docCollection_componentsEverywhere.xml ', function () {
   it('It has to find 9 references', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);

      const n = execResult.references.number;
      expect(n).to.equal(9);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      let ref = execResult.references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let tot = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let used = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by unused test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let unused = JSON.parse(result.substring(0, result.length - 1)).references.list.length;

      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "limeEditor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let eId = JSON.parse(result.substring(0, result.length - 1)).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first unused reference has to be "/lime.cirsfid.unibo.it" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let href = JSON.parse(result.substring(0, result.length - 1)).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last unused reference has to be "Editor" ', function () {
      let result = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-references-info --filter-by used test/docs/document_docCollection_componentsEverywhere.xml').toString());
      let showAs = JSON.parse(result.substring(0, result.length - 1)).references.list[3].showAs;
      expect(showAs).to.equal('Editor');
   });

})


describe('#AkomantosoDocumentHandler Document document_it.xml (Italy)', function() {
   it('After having parsed the XML string the Akn Document Element must exist', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml').toString();
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.nodeName;
      expect(doc).to.equal('akomaNtoso');
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 144094 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml').toString());
      var doc = execResult.length;
      expect(doc).to.equal(144094);
   });
   it('After having parsed the XML string the HTMLString should be long 175703 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s html').toString());
      var doc = execResult.length;
      expect(doc).to.equal(175703);
   });
   it('After having parsed the XML string the JSONString should be long 127533 characters', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s json').toString());
      var doc = execResult.length;
      expect(doc).to.equal(127533);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.nodeType;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(doc).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml --no-data-xpath').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult);
      expect(doc.documentElement.nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 56398 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml --no-data-xpath').toString());
      const result = execResult.length;
      expect(result).to.equal(56398);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -m FRBRthis -s json --no-data-xpath').toString();
      execResult = JSON.parse(execResult);
      const result = execResult.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 56616 character', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -s json --no-data-xpath').toString());
      expect(execResult.length).to.equal(56616);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -m FRBRthis -s html --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 72665 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -s html --no-data-xpath').toString());
      expect(execResult.length).to.equal(72665);
   });
   it('There must be one FRBRcountry element when retreiving meta in akn', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -m FRBRcountry --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(1);
   });
   it('There must be three FRBRthis elements when retreiving meta in akn', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_it.xml -m akn:FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      let result = execSync('node dist/bundle.akomando.bin.js get-doc-type test/docs/document_it.xml').toString();
      result = result.substring(0, result.length - 1);
      expect(result).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml --order descending').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10__para_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml --sort-by name').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml  --sort-by eId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml  --sort-by wId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml  --sort-by xpath').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml --sort-by level').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml --filter-by-name article').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(10);
      expect(execResult[6].eId).to.equal('art_7');
      expect(execResult[execResult.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml --filter-by-content').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(58);
      expect(execResult[0].eId).to.equal('art_1__para_1');
      expect(execResult[57].eId).to.equal('art_10__para_1');
   });
   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_it.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(25020);
   });

   it('Substring from 99 to 110 of the result without new lines must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.substring(99, 110)).to.equal('articolo 2,');
      expect(execResult.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(execResult.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 36174 characters with new lines', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(36174);
   });

   it('The first line of the result with new lines must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_it.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1).split('\n');
      expect(execResult[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a');
      expect(execResult[160]).to.equal('                                            "è soggetto alla sanzione amministrativa pecuniaria');
      expect(execResult[execResult.length-1]).to.equal('                    finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akomaNtoso" and its count must be 1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_it.xml').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements ref must be 97 and element in position 0 must have name "ref" and its count must be 97', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_it.xml --filter-by-name ref').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

   it('The total number of elements mod must be 27 and element in position 0 must have name "mod" and its count must be 27', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_it.xml --filter-by-name mod').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(27);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(27);
   });

   it('The document\'s URIs must be: Work: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo", Expression: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@" and Manifestation: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml"', function() {
      const FRBR = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-frbr test/docs/document_it.xml').toString());
      expect(FRBR.work.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo');
      expect(FRBR.expression.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@');
      expect(FRBR.manifestation.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml');

      expect(FRBR.work.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/!main');
      expect(FRBR.expression.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/!main');
      expect(FRBR.manifestation.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/main.xml/!main');
  });

   it('Tests with getIdReferences: should get 171 id references, 120 existing, 51 pending', function(){
      let result_all = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-id-references test/docs/document_it.xml').toString());
      expect(result_all.length).to.equal(171);

      let result_existing = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-id-references test/docs/document_it.xml --filter-by existing').toString());
      expect(result_existing.length).to.equal(120);
      expect(result_existing[0].refersTo).to.equal('supremaCorteDiCassazione');
      expect(result_existing[0].referenceLinkedInAttribute).to.equal('source');

      let result_pending = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-id-references test/docs/document_it.xml --filter-by pending').toString());
      expect(result_pending.length).to.equal(51);
      expect(result_pending[0].refersTo).to.equal('tesauroCassazione');
   });

   it('Tests with the XPath selection', function() {
       let selection = execSync('node dist/bundle.akomando.bin.js get-elements-from-xpath test/docs/document_it.xml /akomaNtoso/act/body/article').toString();
       expect(selection).to.include('<article eId="art_1"');
       expect(selection).to.include('<article eId="art_2"');
       expect(selection).to.include('<article eId="art_5"');
       expect(selection).to.include('<article eId="art_10"');
       selection = execSync('node dist/bundle.akomando.bin.js get-elements-from-xpath test/docs/document_it.xml /akomaNtoso/act/meta/classification/keyword[2]').toString();
       expect(selection).to.include('<keyword value="CODICE PENALE"');
   });

   it('The work author is stato and publication date is 2016-01-15', function(){
      const publicationInfo = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-publication-info test/docs/document_it.xml --about work').toString());
      expect(publicationInfo.author.name).to.equal('stato');
      expect(publicationInfo.author.ref).to.equal('/ontology/organizations/it/Stato');
      expect(publicationInfo.date).to.equal('2016-01-15');
   });

   it('The expression author is stato and publication date is 2016-01-15', function(){
      const result = execSync('node dist/bundle.akomando.bin.js get-publication-info test/docs/document_it.xml --about expression').toString();
      const publicationInfo = JSON.parse(result);
      expect(publicationInfo.author.name).to.equal('stato');
      expect(publicationInfo.author.ref).to.equal('/ontology/organizations/it/Stato');
      expect(publicationInfo.date).to.equal('2016-01-15');
      /*expect(publicationInfo.publication.date).to.equal('2016-01-22');
      expect(publicationInfo.publication.name).to.equal('GU');
      expect(publicationInfo.publication.showAs).to.equal('Gazzetta Ufficiale');
      expect(publicationInfo.publication.number).to.equal('017');
        */
      const publicationInfoNoAbout = execSync('node dist/bundle.akomando.bin.js get-publication-info test/docs/document_it.xml').toString();
      expect(publicationInfoNoAbout).to.equal(result);
   });

   it('The manifestation author is cirsfid and publication date is 2016-11-13', function(){
      const publicationInfo = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-publication-info test/docs/document_it.xml --about manifestation').toString());
      expect(publicationInfo.author.name).to.equal('CIRSFID');
      expect(publicationInfo.author.ref).to.equal('#http://www.cirsfid.unibo.it/');
      expect(publicationInfo.date).to.equal('2016-11-13');
   });

   it('Should find 59 total id errors: 47 for eId, 12 for wId', function(){
      const totalErrors = execSync('node dist/bundle.akomando.bin.js check-ids test/docs/document_it.xml').toString()
        .split('\n').filter((line) => line.length > 0);
      expect(totalErrors.length).to.equal(59);

      const eIdErrors = execSync('node dist/bundle.akomando.bin.js check-ids test/docs/document_it.xml --id-name eId').toString()
        .split('\n').filter((line) => line.length > 0);
      expect(eIdErrors.length).to.equal(47);

      const wIdErrors = execSync('node dist/bundle.akomando.bin.js check-ids test/docs/document_it.xml --id-name wId').toString()
        .split('\n').filter((line) => line.length > 0);
      expect(wIdErrors.length).to.equal(12);
   });

    it('Tests with getElementById: should get 1 element "textualMod" with eId="_0"', function(){
        let elems = execSync('node dist/bundle.akomando.bin.js get-element-by-id test/docs/document_it.xml _0').toString()
            .split('\n\n')
            .filter((line) => ! /^[\s]*$/.test(line));   // remove empty lines
        expect(elems.length).to.equal(2);     // 1 more for the initial printed line
        expect(elems[1]).to.match(/<textualMod[\s\S]*<\/textualMod>\n/);
    });

    it('Tests with getElementById: should get 1 element "container" with wId="preamble_1__preambolonir_1"', function(){
        let elems = execSync('node dist/bundle.akomando.bin.js get-element-by-id test/docs/document_it.xml preamble_1__preambolonir_1 --id-name wId').toString()
            .split('\n\n')
            .filter((line) => ! /^[\s]*$/.test(line));   // remove empty lines
        expect(elems.length).to.equal(2);     // 1 more for the initial printed line
        expect(elems[1]).to.match(/<container[\s\S]*<\/container>\n/);
    });

    it('Tests with getElementById: should get 0 elements with eId="preamble_1__preambolonir_1"', function(){
        let elems = execSync('node dist/bundle.akomando.bin.js get-element-by-id test/docs/document_it.xml preamble_1__preambolonir_1 --id-name eId').toString()
            .split('\n\n')
            .filter((line) => ! /^[\s]*$/.test(line));   // remove empty lines
        expect(elems.length).to.equal(1);     // 1 more for the initial printed line
        expect(elems[0]).to.contain("0 element(s):");
    });
});

describe('#AkomantosoDocumentHandler Document document_uy.xml (Uruguay)', function() {
   it('After having parsed the XML string the Akn Document Element must exist', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml').toString();
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult.substring(0, execResult.length - 1)).documentElement.nodeName;
      expect(doc).to.equal('akomaNtoso');
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 191654 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml').toString());
      var doc = execResult.length;
      expect(doc).to.equal(191654);
   });
   it('After having parsed the XML string the HTMLString should be long 224246 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s html').toString());
      var doc = execResult.length;
      expect(doc).to.equal(224246);
   });
   it('After having parsed the XML string the JSONString should be long 182245 characters', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s json').toString());
      var doc = execResult.length;
      expect(doc).to.equal(182245);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.nodeType;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(doc).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml --no-data-xpath').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult);
      expect(doc.documentElement.nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 1668 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml --no-data-xpath').toString());
      const result = execResult.length;
      expect(result).to.equal(1668);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -m FRBRthis -s json --no-data-xpath').toString();
      const result = JSON.parse(execResult).length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 1896 character', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -s json --no-data-xpath').toString());
      expect(execResult.length).to.equal(1896);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -m FRBRthis -s html --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 2583 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -s html --no-data-xpath').toString());
      expect(execResult.length).to.equal(2583);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -m FRBRcountry --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(1);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_uy.xml -m akn:FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The document type must be bill', function() {
      let result = execSync('node dist/bundle.akomando.bin.js get-doc-type test/docs/document_uy.xml').toString();
      result = result.substring(0, result.length - 1);
      expect(result).to.equal('bill');
   });

   it('The document must contain 374 hierarchies. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when ordered descending. The eId of the first one must be title_IX__chp_IV__art_83__para_1 and the eId of the last one must be title_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml --order descending').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_IX__chp_IV__art_83__para_1');
      expect(execResult[execResult.length-1].eId).to.equal('title_I');
   });

   it('The document must contain 374 hierarchies when sorted by name. The eId of the first one must be title_I__chp_CAP__art_1 and the eId of the last one must be title_IX', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml --sort-by name').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I__chp_CAP__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('title_IX');
   });

   it('The document must contain 374 hierarchies when sorted by eId. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml  --sort-by eId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by wId. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml  --sort-by wId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by GUID. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by xpath. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml  --sort-by xpath').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by level. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_32__para_1__mod_1__qstr_1__art_1__para_1__content__list_1__item_g', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml --sort-by level').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(374);
      expect(execResult[0].eId).to.equal('title_I');
      expect(execResult[execResult.length-1].eId).to.equal('title_V__chp_CAP__art_32__para_1__mod_1__qstr_1__art_1__para_1__content__list_1__item_g');
   });

   it('The document must contain 94 articles. The eId of the seventh one must be title_II__chp_CAP__art_7 and the eId of the last one must be title_IX__chp_IV__art_83', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml --filter-by-name article').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(94);
      expect(execResult[6].eId).to.equal('title_II__chp_CAP__art_7');
      expect(execResult[execResult.length-1].eId).to.equal('title_IX__chp_IV__art_83');
   });

   it('The document must contain 162 partitions having content. The eId of the first one must be title_I__chp_CAP__art_1__para_1 and the eId of the seventeenth one must be title_IX__chp_IV__art_83__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_uy.xml --filter-by-content').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('title_I__chp_CAP__art_1__para_1');
      expect(execResult[161].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(76369);
   });

   it('Substring from 99 to 110 of the result without new lines must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.substring(99, 110)).to.equal('COS CAPÍTUL');
      expect(execResult.substring(40000, 40020)).to.equal('ienes inmuebles part');
      expect(execResult.substring(65000, 65030)).to.equal(' del ordenante. La instrucción');
   });

   it('The raw text of the document must contain 36174 characters with new lines', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(125041);
   });

   it('The first line of the result with new lines must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_uy.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1).split('\n');
      expect(execResult[0]).to.equal('     Comisión de Hacienda   PROYECTO DE LEY19 de febrero de 2014TÍTULO IDE LOS MEDIOS DE PAGO ELECTRÓNICOS CAPÍTULO ÚNICOArtículo 1º.(Medio de pago');
      expect(execResult[600]).to.equal('                                    probarse a través de la presentación de los recibos de depósito');
      expect(execResult[execResult.length-1]).to.equal('                GONZÁLEZ ALEJANDRO SÁNCHEZ  ');
   });

   it('The total number of elements must be 1737 and element in position 17 must have name "blockList" and its count must be 17', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_uy.xml').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(1737);
      expect(elementsCount.elements[16].name).to.equal('blockList');
      expect(elementsCount.elements[16].count).to.equal(17);
   });

   it('The total number of elements ref must be 34 and element in position 0 must have name "ref" and its count must be 34', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_uy.xml --filter-by-name ref').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(34);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(34);
   });

   it('The total number of elements mod must be 16 and element in position 0 must have name "mod" and its count must be 16', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_uy.xml --filter-by-name mod').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(16);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(16);
   });

   it('The document\'s URIs must be: Work: "/akn/uy/bill/proyectodley/2014-02-07", Expression: "/akn/uy/bill/proyectodley/2014-02-07/spa@" and Manifestation: "/akn/uy/bill/proyectodley/2014-02-07/spa@.xml"', function() {
      const FRBR = JSON.parse(execSync('node dist/bundle.akomando.bin.js get-frbr test/docs/document_uy.xml').toString());
      expect(FRBR.work.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07');
      expect(FRBR.expression.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@');
      expect(FRBR.manifestation.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@.xml');

      expect(FRBR.work.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/!main');
      expect(FRBR.expression.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@/!main');
      expect(FRBR.manifestation.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@/main.xml/!main');
   });
});

describe('#AkomantosoDocumentHandler Document document_unqualified.xml (Italy and unquilified elements)', function() {
   it('After having parsed the XML string the Akn Document Element must exist', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml').toString();
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult.substring(0, execResult.length - 1)).documentElement.nodeName;
      expect(doc).to.equal('akomaNtoso');
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 121044 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml').toString());
      var doc = execResult.length;
      expect(doc).to.equal(121044);
   });
   it('After having parsed the XML string the HTMLString should be long 157869 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s html').toString());
      var doc = execResult.length;
      expect(doc).to.equal(157869);
   });
   it('After having parsed the XML string the JSONString should be long 118899 characters', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s json').toString());
      var doc = execResult.length;
      expect(doc).to.equal(118899);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.nodeType;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(doc).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml --no-data-xpath').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult);
      expect(doc.documentElement.nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 50845 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml --no-data-xpath').toString());
      const result = execResult.length;
      expect(result).to.equal(50845);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -m FRBRthis -s json --no-data-xpath').toString();
      execResult = JSON.parse(execResult);
      const result = execResult.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 88323 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -s json --no-data-xpath').toString());
      expect(execResult.length).to.equal(88323);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 64257 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -s html --no-data-xpath').toString());
      expect(execResult.length).to.equal(64257);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -m FRBRcountry --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(1);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_unqualified.xml -m akn:FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      let result = execSync('node dist/bundle.akomando.bin.js get-doc-type test/docs/document_unqualified.xml').toString();
      result = result.substring(0, result.length - 1);
      expect(result).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml --order descending').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10__para_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml --sort-by name').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml  --sort-by eId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml  --sort-by wId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml  --sort-by xpath').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml --sort-by level').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml --filter-by-name article').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(10);
      expect(execResult[6].eId).to.equal('art_7');
      expect(execResult[execResult.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_unqualified.xml --filter-by-content').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(58);
      expect(execResult[0].eId).to.equal('art_1__para_1');
      expect(execResult[57].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(25020);
   });

   it('Substring from 99 to 110 of the result without new lines must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.substring(99, 110)).to.equal('articolo 2,');
      expect(execResult.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(execResult.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 36174 characters with new lines', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(33261);
   });

   it('The first line of the result with new lines must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_unqualified.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1).split('\n');
      expect(execResult[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. ');
      expect(execResult[170]).to.equal('                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619"');
      expect(execResult[execResult.length-1]).to.equal('                        6.Il pagamento determina l\'estinzione del procedimento.Art. 10Disposizioni finanziarie1.Le amministrazioni interessate provvedono agli adempimenti previsti dal presente decreto, senza nuovi o maggiori oneri a carico della finanza pubblica, con le risorse umane, strumentali e finanziarie disponibili a legislazione vigente.Il presente decreto, munito del sigillo dello Stato, sarà inserito nella Raccolta ufficiale degli atti normativi della Repubblica Italiana. È fatto obbligo a chiunque spetti di osservarlo e di farlo osservare.Dato a Roma, addì 15 gennaio 2016MATTARELLARenzi, Presidente del Consiglio dei ministriOrlando, Ministro della giustiziaPadoan, Ministro dell\'economia e delle finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akomaNtoso" and its count must be 1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_unqualified.xml').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements ref must be 97 and element in position 0 must have name "ref" and its count must be 97', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_unqualified.xml --filter-by-name ref').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

   it('The total number of elements mod must be 27 and element in position 0 must have name "mod" and its count must be 27', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_unqualified.xml --filter-by-name mod').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(27);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(27);
   });
});

describe('#AkomantosoDocumentHandler Document document_qualifiedWithPrefix.xml (Italy, quilified and prefixed elements)', function() {
   it('After having parsed the XML string the Akn Document Element must exist', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml').toString();
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult.substring(0, execResult.length - 1)).documentElement.nodeName;
      expect(doc).to.equal('akn:akomaNtoso');
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 127833 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml').toString());
      var doc = execResult.length;
      expect(doc).to.equal(127833);
   });
   it('After having parsed the XML string the HTMLString should be long 162139 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s html').toString());
      var doc = execResult.length;
      expect(doc).to.equal(162139);
   });
   it('After having parsed the XML string the JSONString should be long 289753 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s json').toString());
      var doc = execResult.length;
      expect(doc).to.equal(289753);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.nodeType;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(doc).to.equal('akn-akn:akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "akn:meta"', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml --no-data-xpath').toString());
      execResult = execResult.substring(0, execResult.length - 1);
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult);
      expect(doc.documentElement.nodeName).to.equal('akn:meta');
   });
   it('The meta in AKN string format must be long 52694 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml --no-data-xpath').toString());
      const result = execResult.length;
      expect(result).to.equal(52694);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -m FRBRthis -s json --no-data-xpath').toString();
      execResult = JSON.parse(execResult);
      const result = execResult.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 89034 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -s json --no-data-xpath').toString());
      expect(execResult.length).to.equal(89034);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 65813 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -s html --no-data-xpath').toString());
      expect(execResult.length).to.equal(65813);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -m FRBRcountry --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(1);
   });
   it('There must be three akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_qualifiedWithPrefix.xml -m akn:FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      let result = execSync('node dist/bundle.akomando.bin.js get-doc-type test/docs/document_qualifiedWithPrefix.xml').toString();
      result = result.substring(0, result.length - 1);
      expect(result).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml --order descending').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10__para_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml --sort-by name').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml  --sort-by eId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml  --sort-by wId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml  --sort-by xpath').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_10');
      expect(execResult[execResult.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml --sort-by level').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(162);
      expect(execResult[0].eId).to.equal('art_1');
      expect(execResult[execResult.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml --filter-by-name article').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(10);
      expect(execResult[6].eId).to.equal('art_7');
      expect(execResult[execResult.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_qualifiedWithPrefix.xml --filter-by-content').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(58);
      expect(execResult[0].eId).to.equal('art_1__para_1');
      expect(execResult[57].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(25020);
   });

   it('Substring from 99 to 110 of the result without new lines must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.substring(99, 110)).to.equal('articolo 2,');
      expect(execResult.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(execResult.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 36174 characters with new lines', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(33261);
   });

   it('The first line of the result with new lines must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_qualifiedWithPrefix.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1).split('\n');
      expect(execResult[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. ');
      expect(execResult[170]).to.equal('                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619"');
      expect(execResult[execResult.length-1]).to.equal('                        6.Il pagamento determina l\'estinzione del procedimento.Art. 10Disposizioni finanziarie1.Le amministrazioni interessate provvedono agli adempimenti previsti dal presente decreto, senza nuovi o maggiori oneri a carico della finanza pubblica, con le risorse umane, strumentali e finanziarie disponibili a legislazione vigente.Il presente decreto, munito del sigillo dello Stato, sarà inserito nella Raccolta ufficiale degli atti normativi della Repubblica Italiana. È fatto obbligo a chiunque spetti di osservarlo e di farlo osservare.Dato a Roma, addì 15 gennaio 2016MATTARELLARenzi, Presidente del Consiglio dei ministriOrlando, Ministro della giustiziaPadoan, Ministro dell\'economia e delle finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akn:akomaNtoso" and its count must be 1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_qualifiedWithPrefix.xml').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akn:akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements akn:ref must be 97 and element in position 0 must have name "akn:ref" and its count must be 97', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_qualifiedWithPrefix.xml --filter-by-name akn:ref').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('akn:ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

   it('The total number of elements akn:mod must be 27 and element in position 0 must have name "akn:mod" and its count must be 27', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_qualifiedWithPrefix.xml --filter-by-name akn:mod').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(27);
      expect(elementsCount.elements[0].name).to.equal('akn:mod');
      expect(elementsCount.elements[0].count).to.equal(27);
   });
});

describe('#AkomantosoDocumentHandler Document document_docCollection.xml (Uruguaian qualified document collection)', function() {
   it('After having parsed the XML string the Akn Document Element must exist', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml').toString();
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult.substring(0, execResult.length - 1)).documentElement.nodeName;
      expect(doc).to.equal('akomaNtoso');
   });
   it('After having parsed the XML string the Akn Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('After having parsed the XML string the AKNString should be long 54304 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml').toString());
      var doc = execResult.length;
      expect(doc).to.equal(54304);
   });
   it('After having parsed the XML string the HTMLString should be long 58672 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s html').toString());
      var doc = execResult.length;
      expect(doc).to.equal(58672);
   });
   it('After having parsed the XML string the JSONString should be long 119385 characters', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s json').toString());
      var doc = execResult.length;
      expect(doc).to.equal(119385);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult.substring(0, execResult.length - 1)).documentElement.nodeType;
      expect(doc).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(doc).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s html').toString());
      var doc = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult).documentElement.childNodes.length;
      expect(doc).to.equal(2);
   });
   it('The must be three meta in AKNDom and their names must be "meta"', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml --no-data-xpath').toString());
      execResult = execResult.substring(0, execResult.length - 1).split(',');
      var doc0 = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult[0]);
      var doc1 = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult[1]);
      var doc2 = new DOMParser({
         errorHandler:{
            warning:function(w){
            }
         }
      }).parseFromString(execResult[2]);
      expect(execResult.length).to.equal(3);
      expect(doc0.documentElement.nodeName).to.equal('meta');
      expect(doc1.documentElement.nodeName).to.equal('meta');
      expect(doc2.documentElement.nodeName).to.equal('meta');
   });
   it('The first meta block in AKN string format must be long 1815 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml --no-data-xpath').toString());
      execResult = execResult.substring(0, execResult.length - 1).split(',');
      const result = execResult[0].length;
      expect(result).to.equal(1815);
   });
   it('There must be nine FRBRthis elements when retreiving meta in AKN format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(9);
   });
   it('There must be nine FRBRthis elements when retreiving meta in JSON format', function() {
      let execResult = execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -m FRBRthis -s json --no-data-xpath').toString();
      execResult = JSON.parse(execResult);
      const result = execResult.length;
      expect(result).to.equal(9);
   });
   it('The first meta block in JSON string format must be long 79 character', function() {
      let execResult = pd.jsonmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -s json --no-data-xpath').toString());
      execResult = execResult.substring(0, execResult.length - 1).split(',');
      expect(execResult[0].length).to.equal(79);
   });
   it('There must be nine FRBRthis elements when retreiving meta in HTML format', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -m FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(9);
   });
   it('The firt meta block in HTML string format must be long 2826 character', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -s html --no-data-xpath').toString());
      execResult = execResult.substring(0, execResult.length - 1).split(',');
      expect(execResult[0].length).to.equal(2826);
   });
   it('There must be three FRBRcountry elements when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -m FRBRcountry --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(3);
   });
   it('There must be nine FRBRthis elements when retreiving meta in AKNDom', function() {
      let execResult = pd.xmlmin(execSync('node dist/bundle.akomando.bin.js get-meta test/docs/document_docCollection.xml -m akn:FRBRthis --no-data-xpath').toString());
      const result = execResult.split(',').length;
      expect(result).to.equal(9);
   });
   it('The document type must be documentCollection', function() {
      let result = execSync('node dist/bundle.akomando.bin.js get-doc-type test/docs/document_docCollection.xml').toString();
      result = result.substring(0, result.length - 1);
      expect(result).to.equal('documentCollection');
   });

   it('The document must contain 59 hierarchies. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when ordered descending. The eId of the first one must be cmpnts__cmp_2__para_1 and the eId of the last one must be cmpnts__cmp_1__art_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml --order descending').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_2__para_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_1__art_1');
   });

   it('The document must contain 59 hierarchies when sorted by name. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml --sort-by name').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1');
   });

   it('The document must contain 59 hierarchies when sorted by eId. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml  --sort-by eId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by wId. The eId of the first one must be cmpnts__cmp_1__art_3__para_1__mod_3 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml  --sort-by wId').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_3__para_1__mod_3');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by GUID. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml  --sort-by GUID').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by xpath. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml  --sort-by xpath').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by level. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_1__art_2__para_1__mod_2__qstr_1__art_2__para_2__list_1__item_C', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml --sort-by level').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(59);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_1__art_2__para_1__mod_2__qstr_1__art_2__para_2__list_1__item_C');
   });

   it('The document must contain 13 articles. The eId of the seventh one must be cmpnts__cmp_1__art_4__para_1__mod_4__qstr_1__art_7 and the eId of the last one must be cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1__art_19', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml --filter-by-name article').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(13);
      expect(execResult[6].eId).to.equal('cmpnts__cmp_1__art_4__para_1__mod_4__qstr_1__art_7');
      expect(execResult[execResult.length-1].eId).to.equal('cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1__art_19');
   });

   it('The document must contain 29 partitions having content. The eId of the first one must be cmpnts__cmp_1__art_1__para_1 and the eId of the seventeenth one must be cmpnts__cmp_2__para_1', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-hier-identifiers test/docs/document_docCollection.xml --filter-by-content').toString();
      let execResult = result.substring(0, result.length - 1);
      execResult = JSON.parse(execResult);
      expect(execResult.length).to.equal(29);
      expect(execResult[0].eId).to.equal('cmpnts__cmp_1__art_1__para_1');
      expect(execResult[28].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(14728);
   });

   it('Substring from 99 to 110 of the result without new lines must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s text --no-new-lines').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.substring(99, 110)).to.equal('ulo 1º.- Mo');
      expect(execResult.substring(10000, 10020)).to.equal('s fueron positivas, ');
      expect(execResult.substring(14000, 14030)).to.equal('sona determinada este proyecto');
   });

   it('The raw text of the document must contain 36174 characters with new lines', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1);
      expect(execResult.length).to.equal(26311);
   });

   it('The first line of the result with new lines must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      let result = execSync('node dist/bundle.akomando.bin.js get-akomantoso test/docs/document_docCollection.xml -s text').toString();
      let execResult = result.substring(0, result.length - 1).split('\n');
      expect(execResult[0]).to.equal('EXTRACCION, CONSERVACION Y TRASPLANTES DE ORGANOS Y TEJIDOS');
      expect(execResult[130]).to.equal('                                                  llevado por cada establecimiento asistencial');
      expect(execResult[execResult.length-1]).to.equal('                         por Canelones ');
   });

   it('The total number of elements must be 451 and element in position 451 must have name "article" and its count must be 13', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_docCollection.xml').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(451);
      expect(elementsCount.elements[16].name).to.equal('article');
      expect(elementsCount.elements[16].count).to.equal(13);
   });

   it('The total number of elements ref must be 11 and element in position 0 must have name "ref" and its count must be 11', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_docCollection.xml --filter-by-name ref').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(11);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(11);
   });

   it('The total number of elements mod must be 7 and element in position 0 must have name "mod" and its count must be 7', function(){
      let result = execSync('node dist/bundle.akomando.bin.js count-doc-elements test/docs/document_docCollection.xml --filter-by-name mod').toString();
      let elementsCount = JSON.parse(result.substring(0, result.length - 1));
      expect(elementsCount.total).to.equal(7);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(7);
   });
});

